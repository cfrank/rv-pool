import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { Link } from 'react-router-dom';
import { Button } from './Button';

describe('src/components/common/button/button.test.jsx', () => {
  const onClick = sinon.spy();
  const childContent = 'This is some text for the child element';
  const wrapper = shallow(
    <Button className="testClassName" label="My Label" onClick={onClick} url="https://www.google.com">
      <p>{childContent}</p>
    </Button>,
  );

  it('should trigger onClick when the button is clicked', () => {
    wrapper.find('button').simulate('click');

    // eslint-disable-next-line no-unused-expressions
    expect(onClick.calledOnce).to.be.true;
  });

  it('should render a Link element for the button', () => {
    expect(wrapper.find(Link)).to.have.length(1);
  });

  it('should render the html button element', () => {
    expect(wrapper.find('button')).to.have.length(1);
  });

  it('should render the correct child element', () => {
    // The `children` prop is the first child of the button element
    const firstButtonChild = wrapper.find('button').children().first();

    expect(firstButtonChild.find('p')).to.have.length(1);
    expect(firstButtonChild.text()).to.eql(childContent);
  });

  it('should render the correct label', () => {
    // The `label` prop is the second child of the button element
    expect(wrapper.find('button').children().at(1).text()).to.eql('My Label');
  });
});
