import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import style from './button.scss';

export const Button = ({ children, className, label, onClick, url }) => (
  <Link to={url} className={className}>
    <button className={style.button} onClick={onClick} type="button">
      {children}
      {label}
    </button>
  </Link>
);

Button.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  url: PropTypes.string.isRequired,
};

Button.defaultProps = {
  className: '',
  onClick: null,
};

export default Button;
