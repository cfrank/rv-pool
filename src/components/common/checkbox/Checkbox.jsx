import React from 'react';
import { PropTypes } from 'prop-types';
import style from './checkbox.scss';

export const Checkbox = ({ checked, id, label, onChange }) => (
  <div className={style.checkboxContainer}>
    <input
      className={style.htmlCheckbox}
      id={id}
      name={id}
      type="checkbox"
      checked={checked}
      onChange={onChange}
    />
    <label className={style.checkbox} htmlFor={id} />
    <label className={style.label} htmlFor={id}>{label}</label>
  </div>
);

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Checkbox;
