import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { Checkbox } from './Checkbox';

describe('src/components/common/checkbox/checkbox.test.jsx', () => {
  const onChange = sinon.spy();
  const id = 'myId';
  const label = 'A checkbox';
  const wrapper = shallow(<Checkbox checked id={id} label={label} onChange={onChange} />);

  it('should call onChange when the input element changes', () => {
    wrapper.find('input').simulate('change');

    // eslint-disable-next-line no-unused-expressions
    expect(onChange.calledOnce).to.be.true;
  });

  it('should render a html input element', () => {
    expect(wrapper.find('input')).to.have.length(1);
  });

  it('should render 2 html label elements', () => {
    expect(wrapper.find('label')).to.have.length(2);
  });

  it('should render the correct label in the second label element', () => {
    expect(wrapper.find('label').at(1).text()).to.eql(label);
  });

  it('should add the correct id to all the elements', () => {
    expect(wrapper.find('input').props()).to.have.property('id', id);
    expect(wrapper.find('label').at(0).props()).to.have.property('htmlFor', id);
    expect(wrapper.find('label').at(1).props()).to.have.property('htmlFor', id);
  });

  it('should add the correct `checked` prop to the input element', () => {
    expect(wrapper.find('input').props()).to.have.property('checked', true);
  });
});
