import React from 'react';
import { PropTypes } from 'prop-types';
import cn from 'classnames';
import style from './input.scss';

export const Input = ({
  autoComplete,
  className,
  input,
  label,
  maxlength,
  meta: { touched, valid, invalid },
  optional,
  type,
  value,
}) => (
  <div className={cn(style.customInput, className)}>
    {!optional
      ? (
        <span
          className={cn(
            style.indicator,
            { [style.error]: touched && invalid,
              [style.valid]: touched && valid,
            })}
        />
      ) : (
        <span className={style.optional}>optional</span>
      )
    }
    <span className={style.label}>{label}</span>
    <input
      autoComplete={autoComplete}
      className={cn(style.input, { [style.error]: touched && invalid })}
      maxLength={maxlength}
      type={type}
      value={value}
      {...input}
    />
  </div>
);

Input.propTypes = {
  autoComplete: PropTypes.string,
  className: PropTypes.string,
  input: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
  ])),
  label: PropTypes.string.isRequired,
  maxlength: PropTypes.number,
  // eslint-disable-next-line react/forbid-prop-types
  meta: PropTypes.object,
  optional: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string,
};

Input.defaultProps = {
  autoComplete: '',
  className: '',
  input: {},
  maxlength: -1,
  meta: {},
  optional: false,
  type: 'text',
  value: '',
};

export default Input;
