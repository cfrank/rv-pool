import React from 'react';
import cn from 'classnames';
import { PropTypes } from 'prop-types';
import style from './modal.scss';

export class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    className: PropTypes.string,
    history: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.number,
      PropTypes.object,
      PropTypes.string,
    ])).isRequired,
    onCloseLocation: PropTypes.string,
  };

  static defaultProps = {
    className: '',
    onCloseLocation: '/',
  };

  componentDidMount() {
    document.querySelector('body').classList.add(style.modalActive);
  }

  componentWillUnmount() {
    document.querySelector('body').classList.remove(style.modalActive);
  }

  onModalClose = () => {
    const { history, onCloseLocation } = this.props;

    history.push(onCloseLocation);
  }

  render() {
    const { className, children } = this.props;

    return (
      <div className={style.modalContainer}>
        <div className={cn(style.modalContent, className)}>
          <div
            className={style.closeButton}
            onClick={this.onModalClose}
            role="button"
            tabIndex={-2}
          >
            <svg width="17" height="17" viewBox="0 0 17 17">
              <g fill="#ffffff" fillRule="evenodd">
                <path d="M14.8 0L0 14.8l2 2L17 2.2" />
                <path d="M17 14.8L2 0 0 2l14.8 15" />
              </g>
            </svg>
          </div>
          {children}
        </div>
      </div>
    );
  }
}

export default Modal;
