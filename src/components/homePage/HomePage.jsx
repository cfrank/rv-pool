import React from 'react';
import { Route } from 'react-router-dom';
import ContactDealer from '~/components/contactDealer';
import DealerSearch from '~/components/dealerSearch';
import Hero from '~/components/hero';
import Steps from '~/components/steps';
import style from './homePage.scss';

export const HomePage = () => (
  <div className={style.homePageContainer}>
    <Hero />
    <Steps />
    <div className={style.waterSeparator} />
    <DealerSearch />
    <Route path="/contact/:dealerId" component={ContactDealer} />
  </div>
);

export default HomePage;
