import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import { appConstants } from '~/appConstants';
import style from './navigationItem.scss';

export class NavigationItem extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element),
    ]),
    itemName: PropTypes.string.isRequired,
    itemLink: PropTypes.string.isRequired,
  };

  static defaultProps = {
    children: undefined,
  };

  state = {
    subNavigationActive: false,
  }

  get isSubNavigationActive() {
    return this.state.subNavigationActive;
  }

  toggleSubNavigation = (event) => {
    if (window.innerWidth <= appConstants.desktopSmallSize) {
      event.preventDefault();
      this.setState({
        subNavigationActive: !this.isSubNavigationActive,
      });
    }
  }

  render() {
    const { children, itemLink, itemName } = this.props;

    return (
      <li className={style.navigationItem}>
        <Link to={itemLink} onClick={children ? this.toggleSubNavigation : null}>
          <span>{itemName}</span>
        </Link>
        {children && (
          <div className={cn(style.subNavigation, { [style.subNavigationActive]: this.isSubNavigationActive })}>
            <ul>
              <li className={cn(style.navigationItem, style.mobileTopLevelNavItem)}>
                <Link to={itemLink}>
                  <span>{itemName}</span>
                </Link>
              </li>
              {children}
            </ul>
          </div>
        )}
      </li>
    );
  }
}

export default NavigationItem;
