import React from 'react';
import { PropTypes } from 'prop-types';
import { NavigationItem } from './navigationItem/NavigationItem';
import style from './navigation.scss';

export const Navigation = ({ isMobileNavActive, toggleMobileNavigation }) => (
  <nav className={isMobileNavActive ? style.mobileNavActive : ''}>
    <div className={style.mobileNavHeader}>
      <p>Menu</p>
      <div className={style.mobileNavClose} onClick={toggleMobileNavigation} role="button" tabIndex={-2}>
        <svg width="17" height="17" viewBox="0 0 17 17">
          <g fill="#03c4c3" fillRule="evenodd">
            <path d="M14.8 0L0 14.8l2 2L17 2.2" />
            <path d="M17 14.8L2 0 0 2l14.8 15" />
          </g>
        </svg>
      </div>
    </div>
    <ul>
      <NavigationItem itemName="Pools &amp; Spas" itemLink="#pools-spas">
        <NavigationItem itemName="All Products" itemLink="#all-products" />
        <NavigationItem itemName="Spas" itemLink="#spas" />
        <NavigationItem itemName="Indoor Pools" itemLink="#indoor-pools" />
        <NavigationItem itemName="Lighting" itemLink="#lighting" />
        <NavigationItem itemName="Outdoor Pools" itemLink="#outdoor-pools" />
        <NavigationItem itemName="Water Features" itemLink="#water-features" />
        <NavigationItem itemName="Concrete Pools" itemLink="#concrete-pools" />
        <NavigationItem itemName="Custom Features" itemLink="#custom-features" />
        <NavigationItem itemName="Fiberglass Pools" itemLink="#fiberflass-pools" />
        <NavigationItem itemName="Tile &amp; Mosaic" itemLink="#tile-mosaic" />
      </NavigationItem>
      <NavigationItem itemName="Supplies" itemLink="#supplies">
        <NavigationItem itemName="Filters" itemLink="#filters" />
        <NavigationItem itemName="PH Balance Kits" itemLink="#ph-balance-kits" />
        <NavigationItem itemName="Automatic Cleaners" itemLink="#automatic-cleaners" />
        <NavigationItem itemName="Heaters" itemLink="#heaters" />
      </NavigationItem>
      <NavigationItem itemName="Resources" itemLink="#resources">
        <NavigationItem itemName="Salt vs. Chlorine" itemLink="#salt-vs-chlorine" />
        <NavigationItem itemName="Financing" itemLink="#financing" />
        <NavigationItem itemName="Investment Information" itemLink="#investment-information" />
        <NavigationItem itemName="Warranties" itemLink="#warranties" />
        <NavigationItem itemName="Design Gallery" itemLink="#design-gallery" />
        <NavigationItem itemName="FAQ" itemLink="#faq" />
      </NavigationItem>
      <NavigationItem itemName="Services" itemLink="#services">
        <NavigationItem itemName="Pool Service" itemLink="#pool-service" />
        <NavigationItem itemName="Pool Installation" itemLink="#pool-installation" />
        <NavigationItem itemName="Spa Service" itemLink="#spa-service" />
        <NavigationItem itemName="Spa Installation" itemLink="#spa-installation" />
      </NavigationItem>
    </ul>
  </nav>
);

Navigation.propTypes = {
  isMobileNavActive: PropTypes.bool.isRequired,
  toggleMobileNavigation: PropTypes.func.isRequired,
};

export default Navigation;
