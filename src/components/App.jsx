import React from 'react';
import PropTypes from 'prop-types';
import Footer from '~/components/footer';
import Header from '~/components/header';
import style from './app.scss';

export const App = ({ children }) => (
  <div className={style.appContainer}>
    <Header />
    <main>
      {children}
    </main>
    <Footer />
  </div>
);

App.propTypes = {
  children: PropTypes.element.isRequired,
};

export default App;
