import React from 'react';
import configureStore from 'redux-mock-store';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import dealersData from '~/dealers';
import { Dealer } from './dealer/Dealer';
import { DealerFilter } from './dealerFilter/DealerFilter';
import { DealerSearch } from './DealerSearch';

describe('src/components/dealerSearch/dealerSearch.test.jsx', () => {
  const mockStore = configureStore();
  const state = {
    poolPros: {
      dealersData: {
        dealers: dealersData.dealers,
      },
    },
  };
  const store = mockStore(state);
  const wrapper = shallow(<DealerSearch store={store} />);
  const innerWrapper = wrapper.dive();

  it('should render a DealerFilter element', () => {
    expect(innerWrapper.find(DealerFilter)).to.have.length(1);
  });

  it('should render all dealers when no active filter is present', () => {
    expect(innerWrapper.find(Dealer)).to.have.length(dealersData.dealers.length);
  });

  it('should render the correct amount of dealers with acttive filters', () => {
    const activeFilters = ['Service Pro', 'Residential Pro'];
    activeFilters.forEach((filter) => {
      // Make sure there isn't any problems with async setState as well
      innerWrapper.instance().activateFilter(filter);
    });

    expect(innerWrapper.find(Dealer)).to.have.length(4);
  });

  it('should render the correct amount of dealers when one active filter is removed', () => {
    expect(innerWrapper.find(Dealer)).to.have.length(4);

    innerWrapper.instance().deactivateFilter('Service Pro');

    expect(innerWrapper.find(Dealer)).to.have.length(6);
  });

  it('should correctly handle a filter change event', () => {
    const filterEvent = {
      target: {
        checked: true,
      },
    };

    expect(innerWrapper.find(Dealer)).to.have.length(6);

    innerWrapper.instance().handleFilterChange(filterEvent, 'Installation Pro');
    innerWrapper.instance().handleFilterChange(filterEvent, 'Commercial Pro');

    expect(innerWrapper.find(Dealer)).to.have.length(4);
  });
});
