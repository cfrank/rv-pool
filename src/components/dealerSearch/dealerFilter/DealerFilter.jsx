import React from 'react';
import { PropTypes } from 'prop-types';
import _ from 'lodash';
import { Checkbox } from '~/components/common';
import style from './dealerFilter.scss';

const filters = [
  { name: 'service', certification: 'Service Pro' },
  { name: 'installation', certification: 'Installation Pro' },
  { name: 'residential', certification: 'Residential Pro' },
  { name: 'commercial', certification: 'Commercial Pro' },
];

export const DealerFilter = ({ activeFilters, dealerCount, onChange }) => (
  <div className={style.filters}>
    <p className={style.dealerCountLabel}>{dealerCount} dealers</p>
    <div className={style.filterResultsLabel}>
      <p>Filter Results</p>
    </div>
    <div className={style.filterResult}>
      <ul>
        {_.forEach(filters, filter => (
          <li key={filter.name}>
            <Checkbox
              checked={activeFilters.includes(filter.certification)}
              id={filter.name}
              label={filter.certification}
              onChange={event => onChange(event, filter.certification)}
            />
          </li>
        ))}
      </ul>
    </div>
  </div>
);

DealerFilter.propTypes = {
  activeFilters: PropTypes.arrayOf(PropTypes.string).isRequired,
  dealerCount: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

DealerFilter.defaultProps = {
  activeFilters: [],
};

export default DealerFilter;
