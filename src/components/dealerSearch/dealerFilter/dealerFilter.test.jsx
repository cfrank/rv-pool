import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { Checkbox } from '~/components/common';
import { DealerFilter } from './DealerFilter';

describe('src/components/dealerSearch/dealerFilter/dealerFilter.test.jsx', () => {
  const activeFilters = ['Service Pro', 'Commercial Pro'];
  const onChange = sinon.spy();
  const wrapper = shallow(
    <DealerFilter
      activeFilters={activeFilters}
      dealerCount={2}
      onChange={onChange}
    />,
  );

  it('should render the correct dealer count', () => {
    expect(wrapper.find('p').first().text()).to.eql('2 dealers');
  });

  it('should render the correct number of filters', () => {
    expect(wrapper.find(Checkbox)).to.have.length(4);
  });
});
