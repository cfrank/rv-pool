import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Dealer } from './dealer/Dealer';
import { DealerFilter } from './dealerFilter/DealerFilter';
import style from './dealerSearch.scss';

@connect(state => ({
  dealers: state.poolPros.dealersData.dealers,
}))
export class DealerSearch extends React.Component {
  static propTypes = {
    dealers: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    dealers: [],
  };

  state = {
    activeFilters: [],
  }

  get activeFilters() {
    const { activeFilters } = this.state;

    return activeFilters;
  }

  get activeDealers() {
    const { dealers } = this.props;

    if (this.activeFilters.length === 0) {
      return dealers;
    }

    return dealers.filter(dealer => (
      this.activeFilters.every(filter => (
        dealer.data.certifications.indexOf(filter) >= 0
      ))
    ));
  }

  handleFilterChange = (event, filter) => (
    // If the checkbox is checked activate it
    event.target.checked
      ? this.activateFilter(filter)
      : this.deactivateFilter(filter)
  )

  deactivateFilter(filter) {
    this.setState({
      activeFilters: this.activeFilters.filter(filterValue => filterValue !== filter),
    });
  }

  activateFilter(filter) {
    this.setState({
      activeFilters: [...this.activeFilters, filter],
    });
  }


  render() {
    const { activeFilters } = this.state;

    return (
      <section className={style.dealerSearchContainer}>
        <DealerFilter
          activeFilters={activeFilters}
          dealerCount={this.activeDealers.length}
          onChange={this.handleFilterChange}
        />
        <div className={style.dealerList}>
          <ul>
            {this.activeDealers.map(dealer => (
              <li key={dealer.data.companyID}>
                <Dealer dealer={dealer.data} />
              </li>
            ))}
          </ul>
        </div>
      </section>
    );
  }
}

export default DealerSearch;
