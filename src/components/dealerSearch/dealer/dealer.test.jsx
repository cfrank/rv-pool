import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Button } from '~/components/common';
import { Dealer } from './Dealer';

const dealer = {
  companyID: 401929,
  name: 'Aqua Experts',
  phone1: '1-888-234-5678',
  email: 'info@aquaexperts.com',
  addressLine1: '1234 Some Street',
  addressLine2: '',
  city: 'Charlotte',
  state: 'North Carolina',
  country: 'US',
  zipcode: '28205',
  weekHours: {
    mon: '7:00am - 7:00pm',
    tue: '7:00am - 7:00pm',
    wed: '7:00am - 7:00pm',
    thu: '7:00am - 7:00pm',
    fri: '7:00am - 7:00pm',
    sat: '7:00am - 3:00pm',
    sun: '',
  },
  certifications: [
    'Installation Pro',
    'Residential Pro',
    'Service Pro',
  ],
};

describe('src/components/dealerSearch/dealer/dealer.test.jsx', () => {
  const wrapper = shallow(<Dealer dealer={dealer} />);

  it('should render the correct dealer name', () => {
    expect(wrapper.find('h3').text()).to.eql(dealer.name);
  });

  it('should render the correct dealer phone number with the correct format', () => {
    expect(wrapper.find('p').first().text()).to.eql(dealer.phone1.replace(/-/g, '.'));
  });

  it('should render a Button element', () => {
    expect(wrapper.find(Button)).to.have.length(1);
  });

  it('should render the correct billing hours', () => {
    const billingHours = wrapper.find('ul').first().find('li');

    expect(billingHours).to.have.length(3);
    expect(billingHours.at(0).text()).to.eql('Weekdays 7:00am - 7:00pm');
    expect(billingHours.at(1).text()).to.eql('Saturday 7:00am - 3:00pm');
    expect(billingHours.at(2).text()).to.eql('Sunday Closed');
  });

  it('should render the correct certifications', () => {
    const certifications = wrapper.find('ul').at(1).find('li');

    expect(certifications).to.have.length(3);
  });
});
