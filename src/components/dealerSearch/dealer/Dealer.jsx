import React from 'react';
import { PropTypes } from 'prop-types';
import { Button } from '~/components/common';
import installationIcon from './certification-icons/installation.png';
import commercialIcon from './certification-icons/commercial.png';
import residentialIcon from './certification-icons/residential.png';
import serviceIcon from './certification-icons/service.png';
import style from './dealer.scss';

const certificationIcons = new Map([
  ['Installation Pro', installationIcon],
  ['Commercial Pro', commercialIcon],
  ['Residential Pro', residentialIcon],
  ['Service Pro', serviceIcon],
]);

const weekdayLookup = new Map([
  ['mon', 'Monday'],
  ['tue', 'Tuesday'],
  ['wed', 'Wednesday'],
  ['thu', 'Thursday'],
  ['fri', 'Friday'],
  ['sat', 'Saturday'],
  ['sun', 'Sunday'],
]);

const generateBillingHours = (weekHours) => {
  const billingHours = [];

  Object.keys(weekHours).forEach((day) => {
    if (billingHours.length === 0 || weekHours[day] !== billingHours[billingHours.length - 1].time) {
      billingHours.push({
        start: day,
        end: day,
        time: weekHours[day] === '' ? 'Closed' : weekHours[day],
      });
    } else {
      const previousDay = billingHours.pop();
      billingHours.push({
        start: previousDay.start,
        end: day,
        time: weekHours[day] === '' ? 'Closed' : weekHours[day],
      });
    }
  });

  return billingHours;
};

const renderHours = (billingHours) => {
  if (billingHours.start === 'mon' && billingHours.end === 'fri') {
    return `Weekdays ${billingHours.time}`;
  }

  if (billingHours.start === 'sat' && billingHours.end === 'sun') {
    return `Weekends ${billingHours.time}`;
  }

  if (billingHours.start === billingHours.end) {
    return `${weekdayLookup.get(billingHours.start)} ${billingHours.time}`;
  }

  return `${weekdayLookup.get(billingHours.start)} - ${weekdayLookup.get(billingHours.end)} ${billingHours.time}`;
};

export const Dealer = ({ dealer }) => (
  <div className={style.dealerContainer}>
    <div className={style.dealerTitle}>
      <h3>
        {dealer.name}
      </h3>
    </div>
    <span className={style.seperator} />
    <p>
      {dealer.phone1.replace(/-/g, '.')}
    </p>
    <span className={style.hint}>
      Can&apos;t talk now? Click below to send an email.
    </span>
    <Button
      className={style.contactProButton}
      label="Contact this Pro"
      tabIndex={-2}
      url={`/contact/${dealer.companyID}`}
    >
      <svg width="16" height="14" viewBox="0 0 16 14" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M10.18 9l5.3-4.05v7.43c0 .27-.2.5-.48.52-2.3.33-4.67.5-7 .5s-4.7-.17-7.02-.5c-.25-.03-.45-.25-.45-.52V4.95L5.83 9c.6.47 1.4.67 2.17.67.78 0 1.57-.2 2.18-.67zM1.12 3.27c-.4-.4-.54-.7-.6-.9v-.75c0-.27.2-.5.46-.52C3.3.77 5.65.6 8 .6c2.35 0 4.7.17 7 .5.27.03.47.25.47.52v.76c-.05.2-.2.5-.6.9L9.2 7.58c-.32.23-.75.34-1.2.34-.45 0-.88-.1-1.2-.36l-5.68-4.3z"
          fill="#083D8C"
          fillRule="evenodd"
        />
      </svg>
    </Button>
    <div className={style.businessHours}>
      <span>
        Business Hours
      </span>
      <ul>
        {generateBillingHours(dealer.weekHours).map(hours => (
          <li key={hours.start}>
            <p>{renderHours(hours)}</p>
          </li>
        ))}
      </ul>
    </div>
    <div className={style.certifications}>
      <ul>
        {dealer.certifications.map(certification => (
          <li key={certification}>
            <img
              src={certificationIcons.get(certification)}
              alt={`${certification} icon`}
            />
            {certification}
          </li>
        ))}
      </ul>
    </div>
  </div>
);

Dealer.propTypes = {
  dealer: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.objectOf(PropTypes.string),
      PropTypes.arrayOf(PropTypes.string),
    ]),
  ).isRequired,
};

export default Dealer;
