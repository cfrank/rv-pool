import React from 'react';
import style from './hero.scss';

export const Hero = () => (
  <section className={style.heroContainer}>
    <hgroup>
      <h1>Is your pool ready for summer?</h1>
      <h2>A pool pro is ready to help!</h2>
    </hgroup>
    <span className={style.heroSeparator} />
    <p>Choose a pro that&apos;s close to your home from the list below.</p>
  </section>
);

export default Hero;
