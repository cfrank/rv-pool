import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '~/components/common';
import Navigation from '~/components/navigation';
import logo from './logo.png';
import style from './header.scss';

export class Header extends React.Component {
  state = {
    mobileNavActive: false,
  }

  get isMobileNavActive() {
    return this.state.mobileNavActive;
  }

  toggleMobileNavActive = () => {
    this.setState({
      mobileNavActive: !this.isMobileNavActive,
    });
  }

  render() {
    return (
      <header>
        <section className={style.subHeading}>
          <ul>
            <li>
              <Link to="#dealers-distributors">
                Dealers and Deistributors
              </Link>
            </li>
            <li className={style.commercialService}>
              <Link to="#commercial-services">
                Commercial Service
              </Link>
            </li>
          </ul>
        </section>
        <section className={style.heading}>
          <div className={style.headingContainer}>
            <div className={style.logo}>
              <Link to="/">
                <img alt="Pool Pros Logo" src={logo} />
              </Link>
            </div>
            <Navigation isMobileNavActive={this.isMobileNavActive} toggleMobileNavigation={this.toggleMobileNavActive} />
            <Button className={style.poolProButton} label="Find a Pool Pro" tabIndex={0} url="#find-pool-pro">
              <svg width="13" height="18" viewBox="0 0 13 18" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M6.4849372.579793c3.3308097 0 6.043531 2.85007428 6.043531 6.31823695 0 4.73867775-5.1507366 9.66621575-5.1507366 9.66621575-.7382723.6867649-1.1503312.6352575-1.8370961 0 0 0-5.0992293-4.927538-5.0992293-9.66621575 0-3.46816267 2.7127214-6.31823695 6.043531-6.31823695zm0 8.8936053c1.5795593 0 2.8672434-1.27051506 2.8672434-2.8500743 0-1.57955924-1.2876841-2.8672434-2.8672434-2.8672434-1.5795592 0-2.8500742 1.28768416-2.8500742 2.8672434s1.270515 2.8500743 2.8500742 2.8500743z"
                  fill="#216DDF"
                  fillRule="evenodd"
                />
              </svg>
            </Button>
            <div className={style.mobileNavToggle} onClick={this.toggleMobileNavActive} role="button" tabIndex={-1}>
              <span />
              <span />
              <span />
            </div>
          </div>
        </section>
      </header>
    );
  }
}

export default Header;
