import React from 'react';
import { PropTypes } from 'prop-types';
import style from './step.scss';

export const Step = ({ description, number, title }) => (
  <div className={style.stepContainer}>
    <div className={style.circle}>
      {number}
    </div>
    <div className={style.information}>
      <p className={style.infoTitle}>{title}</p>
      <p className={style.infoDescription}>{description}</p>
    </div>
  </div>
);

Step.propTypes = {
  description: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default Step;
