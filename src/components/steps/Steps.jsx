import React from 'react';
import { Step } from './step/Step';
import style from './steps.scss';

export const Steps = () => (
  <section className={style.stepsContainer}>
    <ul>
      <li>
        <Step
          number={1}
          title="Choose a pro"
          description="Make sure your ZIP code is correct, then choose a pro from the list below."
        />
      </li>
      <li>
        <Step
          number={2}
          title="Contact a pro"
          description="Email a pro to make an appointment for pool &amp; spa services or installation."
        />
      </li>
      <li>
        <Step
          number={3}
          title="A pro visits"
          description="A pro will visit your home and assess your pool &amp; spa needs."
        />
      </li>
    </ul>
  </section>
);

export default Steps;
