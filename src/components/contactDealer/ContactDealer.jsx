import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { clearDealerDetail, fetchDealerDetail } from '~/actions';
import { Modal } from '~/components/common';
import ContactDealerForm from './contactDealerForm/ContactDealerForm';
import style from './contactDealer.scss';

@connect(state => ({
  dealerDetail: state.poolPros.dealerDetail,
}))
export class ContactDealer extends React.Component {
  static propTypes = {
    dealerDetail: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.number,
      PropTypes.object,
      PropTypes.string,
    ])),
    dispatch: PropTypes.func.isRequired,
    history: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.number,
      PropTypes.object,
      PropTypes.string,
    ])).isRequired,
    match: PropTypes.objectOf(PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.object,
      PropTypes.string,
    ])).isRequired,
    onCloseLocation: PropTypes.string,
  };

  static defaultProps = {
    dealerDetail: {},
    onCloseLocation: '',
  };

  componentDidMount() {
    const { dispatch, match } = this.props;

    dispatch(fetchDealerDetail(match.params.dealerId));
  }

  componentWillUnmount() {
    const { dispatch } = this.props;

    dispatch(clearDealerDetail());
  }

  render() {
    const { dealerDetail, history, onCloseLocation } = this.props;

    if (Object.keys(dealerDetail).length === 0 && dealerDetail.constructor === Object) {
      return null;
    }

    return (
      <Modal
        className={style.contactDealerContent}
        history={history}
        onCloseLocation={onCloseLocation}
      >
        <ContactDealerForm dealerDetail={dealerDetail} />
      </Modal>
    );
  }
}

export default ContactDealer;
