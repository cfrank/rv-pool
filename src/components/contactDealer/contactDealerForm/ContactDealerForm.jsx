import React from 'react';
import { PropTypes } from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Input } from '~/components/common';
import style from './contactDealerForm.scss';

const FORM_TOKEN = 'contact-dealer-form';

const required = value => (value ? undefined : 'Required');
const telephone = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? 'Invalid Phone Number'
    : undefined;
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid Email Address'
    : undefined;

export const ContactDealerForm = ({ dealerDetail, handleSubmit, pristine, valid }) => (
  <div className={style.contactDealerForm}>
    <div className={style.header}>
      <span>Email</span>
      <h4>{dealerDetail.name}</h4>
    </div>
    <div className={style.body}>
      <p className={style.instructions}>Fill out the form below and {dealerDetail.name} will get in touch.</p>
      <div className={style.formBody}>
        <form onSubmit={handleSubmit} className={style.form}>
          <Field
            name="name"
            label="First and last name"
            className={style.dealerFormName}
            component={Input}
            validate={[required]}
          />
          <Field
            name="phone"
            label="Phone number"
            className={style.dealerFormPhone}
            component={Input}
            maxlength={10}
            type="tel"
            validate={[required, telephone]}
          />
          <Field
            name="email"
            label="Email Address"
            className={style.dealerFormEmail}
            component={Input}
            type="email"
            validate={[required, email]}
          />
          <Field
            name="comments"
            label="Comments or questions"
            className={style.dealerFormComments}
            component={Input}
            optional
          />
          <div className={style.currentlyOwns}>
            <span className={style.mainLabel}>Do you currently own a pool or spa?</span>
            <Field name="currentlyOwns" id="yes" value="yes" component="input" type="radio" />
            <label className={style.label} htmlFor="yes">Yes</label>
            <Field name="currentlyOwns" id="no" value="no" component="input" type="radio" />
            <label className={style.label} htmlFor="no">No</label>
          </div>
          <span className={style.separator} />
          <button type="submit" disabled={pristine || !valid}>Send my email</button>
        </form>
      </div>
    </div>
    <div className={style.formInformation}>
      <p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex.&quot;</p>
    </div>
  </div>
);

ContactDealerForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
  dealerDetail: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
};

const form = reduxForm({
  form: FORM_TOKEN,
});

export default form(ContactDealerForm);
