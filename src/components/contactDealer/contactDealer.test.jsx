import React from 'react';
import configureStore from 'redux-mock-store';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import dealersData from '~/dealers';
import { Modal } from '~/components/common';
import { clearDealerDetail, fetchDealerDetail } from '~/actions';
import { ContactDealer } from './ContactDealer';

describe('src/components/contactDealer/contactDealer.test.jsx', () => {
  const mockStore = configureStore();
  const state = {
    poolPros: {
      dealerDetail: dealersData.dealers[0].data,
    },
  };
  const store = mockStore(state);
  sinon.stub(store, 'dispatch');
  const history = {};
  const match = {
    params: {
      dealerId: '123',
    },
  };
  const wrapper = shallow(<ContactDealer store={store} history={history} match={match} />);
  const innerWrapper = wrapper.dive();

  it('should dispatch #fetchDealerDetail on #componentDidMount', () => {
    innerWrapper.instance().componentDidMount();

    // eslint-disable-next-line no-unused-expressions
    expect(store.dispatch.calledOnce).to.be.true;
    expect(store.dispatch.calledWith(fetchDealerDetail(match.params.dealerId)));
  });

  it('should dispatch #clearDealerDetail on #componentWillUnmount', () => {
    store.dispatch.reset();

    innerWrapper.instance().componentWillUnmount();

    // eslint-disable-next-line no-unused-expressions
    expect(store.dispatch.calledOnce).to.be.true;
    expect(store.dispatch.calledWith(clearDealerDetail()));
  });

  it('should render a Modal element', () => {
    expect(innerWrapper.find(Modal)).to.have.length(1);
  });

  it('should render a ContactDealerForm element', () => {
    // Ugh.. Not sure how to make this better. Maybe mounting it?
    expect(innerWrapper.find('ReduxForm')).to.have.length(1);
  });
});
