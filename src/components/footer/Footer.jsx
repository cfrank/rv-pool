import React from 'react';
import logo from './logo.png';
import facebook from './social-icons/facebook.svg';
import twitter from './social-icons/twitter.svg';
import youtube from './social-icons/youtube.svg';
import style from './footer.scss';

export const Footer = () => (
  <footer className={style.footerContainer}>
    <div className={style.foot}>
      <img width="188" height="24" src={logo} alt="Pool Pros Logo" />
      <div className={style.social}>
        <span>Connect with us</span>
        <ul>
          <li>
            <a href="https://www.twitter.com">
              <img src={twitter} alt="Follow us on Twitter!" />
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com">
              <img src={facebook} alt="Like us on Facebook!" />
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com">
              <img src={youtube} alt="Subscribe to our YouTube channel!" />
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div className={style.subFooter}>
      <p className={style.subFooterCopy}>
        <span>&copy; {new Date().getFullYear()} Pool Pros</span>
        <span className={style.seperator}>|</span>
        <a href="#privacy-policy">Privacy Policy</a>
        <span className={style.seperator}>|</span>
        <a href="#terms-and-conditions">Terms and Conditions</a>
      </p>
    </div>
  </footer>
);

export default Footer;
