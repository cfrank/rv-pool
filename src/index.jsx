import React from 'react';
import { applyMiddleware, createStore, compose } from 'redux';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, connectRouter, routerMiddleware } from 'connected-react-router';
import App from './components/App';
import HomePage from './components/homePage';
import rootReducer from './reducer';

const history = createHistory();
const store = createStore(
  connectRouter(history)(rootReducer),
  {},
  compose(
    applyMiddleware(routerMiddleware(history)),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App>
        <HomePage />
      </App>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
