import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import dealersData from './dealers';
import { CLEAR_DEALER_DETAIL, FETCH_DEALER_DETAIL } from './actionConstants';

const defaultDealersState = {
  dealersData,
  dealerDetail: {},
};

export function dealersReducer(state = defaultDealersState, action) {
  switch (action.type) {
    case CLEAR_DEALER_DETAIL:
      return {
        ...state,
        dealerDetail: {},
      };
    case FETCH_DEALER_DETAIL:
      return {
        ...state,
        dealerDetail: state.dealersData.dealers.find(dealer => (
          dealer.data.companyID === parseInt(action.companyID, 10)),
        ).data || {},
      };
    default:
      // No OP
      return state;
  }
}

const rootReducer = combineReducers({
  poolPros: dealersReducer,
  form: formReducer,
});

export default rootReducer;
