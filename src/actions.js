import { CLEAR_DEALER_DETAIL, FETCH_DEALER_DETAIL } from './actionConstants';

export function clearDealerDetail() {
  return {
    type: CLEAR_DEALER_DETAIL,
  };
}

export function fetchDealerDetail(companyID) {
  return {
    type: FETCH_DEALER_DETAIL,
    companyID,
  };
}
