const merge = require('webpack-merge');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const common = require('./webpack.common');

module.exports = merge(common, {
  devtool: 'cheap-source-map',

  devServer: {
    compress: true,
    contentBase: path.join(__dirname, 'src'),
    historyApiFallback: true,
    port: 8080,
    stats: 'errors-only',
    watchContentBase: true,
    watchOptions: {
      ignored: /node_modules/,
      poll: 1000,
    },
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
});
